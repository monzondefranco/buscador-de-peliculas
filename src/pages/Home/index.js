import React, {useState} from 'react'
import { Container, Card, Grid, Typography, TextField, Button } from '@material-ui/core'
import { MovieIcon } from '../../Icons'
import styles from './styles'

function Home() {
    const [searchText, setSearchText] = useState('')
    const classes = styles()
    
    const handleSearchTextChange = event => {
        setSearchText(event.target.value);
    }

    const handleCleanTextClick = event => {
        console.log(10)
    }

    const handleSearchTextClick = event => {
        console.log(10)
    }
    
	return (
		<Container className={classes.container}>
			<Card className={classes.cardContainer}>
                <Grid container className={classes.tittleGridContainer}>
                    <Grid>
                        <Typography className={classes.tittle}>Bienvenido!</Typography>
                    </Grid>
                    <Grid>
                        <MovieIcon className={classes.movieIcon} />
                    </Grid>
                </Grid>
                <TextField
                    value={searchText}
                    placeholder="Buscar..."
                    className={classes.testFieldSearch}
                    onChange={handleSearchTextChange} />
                <Grid className={classes.buttonContainer}>
                    <Button variant='contained' onClick={handleCleanTextClick}>Limpiar</Button>
                    <Button variant='contained' color='primary' size='large' className={classes.searchButton} onClick={handleSearchTextClick}>Buscar</Button>
                </Grid>
            </Card>
		</Container>
	)
}

export default Home